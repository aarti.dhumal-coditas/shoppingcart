/**
 * 
 * @author Aarti
 * Purpose: This is all about Shopping cart application.
 * It have methods for adding items to the cart, 
 * removing items, calculating the total price, and displaying the items in the cart.
 */
public class Main {
    public static void main(String[] args) {
        ShoppingCart cart = new ShoppingCart();

        Item item1 = new Item("Item 1", 5);
        Item item2 = new Item("Item 2", 10);
        Item item3 = new Item("Item 3", 15);

        cart.addItem(item1);
        cart.addItem(item2);
        cart.addItem(item3);

        cart.displayItems();

        double totalPrice = cart.calculateTotalPrice();
        System.out.println("Total Price: Rupees" + totalPrice);

        cart.removeItem(item2);

        cart.displayItems();

        totalPrice = cart.calculateTotalPrice();
        System.out.println("Updated Total Price: Rupees" + totalPrice);
    }
}
